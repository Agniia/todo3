<?php include 'template/header.php'; ?>
<h2><?php echo $header; ?></h2>
  <table class="tasks table table-dark">
    <tr>
        <td>Номер задачи</td>
        <td>Описание задачи</td>
        <td>Кто поставил задачу</td>
        <td>Ответственный</td>
        <td>Статус</td>
        <td>Дата добавления</td>
        <td>Изменить</td>
    </tr>
       <?php  foreach($tasks as $task):?>
            <tr>
            <?php if($task['owner_id'] == $_SESSION['user_id'])
                        $action = 'updateResponsibleAndStatus';   
                            else 
                        $action = 'updateStatus';   
            ?>
            <form method="post" action="index.php?c=task&a=<?php echo $action; ?>&id=<?php echo $_SESSION['user_id']; ?>">
                <input type="hidden" name="task_id" value="<?php echo $task['task_id']; ?>">
                <td><?php echo $task['task_id']; ?></td>
                <td><?php echo $task['task_description']; ?></td>
                <td><?php echo $task['owner_login']; ?></td>
                <?php if($task['owner_id'] == $_SESSION['user_id']):?>
                     <td><input type="hidden" name="assigned_user_id" value="<?php echo $task['assigned_user_id']; ?>">
                    <select name="assigned_user_id">
                        <option value="<?php echo $task['assigned_user_id']; ?>"><?php echo $task['responsible_login']; ?></option>
                        <?php foreach($all_users as $user):?>
                        <option value="<?php echo $user['user_id']; ?>"><?php echo $user['owner_login']; ?></option>
                        <?php endforeach;?>
                    </select>
                 <?php else:?>   
                     <td><?php echo $task['responsible_login']; ?></td>
                <?php endif;?>
                <td>
                <select name="status">
                 <?php if($task['status'] == 1):?>
                        <option value="0">Не выполено</option>
                        <option value="1" selected>Выполнено</option>
                 <?php else:?> 
                        <option value="0" selected>Не выполено</option>
                        <option value="1" >Выполнено</option>
                 <?php endif;?>
                </select>
                 </td>
                <td><?php echo $task['date_added']; ?></td>
                <td><button class="btn btn-secondary" type="submit" name="update">Изменить</button></td>
             </form>
            </tr>
        <?php endforeach; ?>
  </table>
 <?php  if($can_add_task == 1):?>
    <h2>Добавить задачу</h2>
     <form action="index.php?c=task&a=add&id=<?php echo $_SESSION['user_id']; ?>" method="post" id="new_task">
        <input type="text" name="description" placeholder="Описание задачи" required> <button class="btn btn-primary" type="submit"  form="new_task">Добавить задачу</button>
     </form>
 <?php endif; ?>