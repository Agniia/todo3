<!DOCTYPE html>
	<html><head><meta charset="utf-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><title>Задачи</title>	
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	<style>
	    html,body{
	        height: 100%;
	    }
	    div, ul, p{
	         padding: 0;
	         margin: 0;
	    }
        h1, form, .tasks{
            width: 80%;
            margin: 50px auto 40px;
        }
        h1, h2, form{
            text-align: center;
        }
        h2{
        	margin: 50px auto 40px;
        }
        header, footer{
            color: #fff;
            font-size: 0.8rem;
            background: #000;
            height: 100px;
        }
        .horizontal-padding{
            padding-left: 25px;
            padding-right: 25px;
        }
        .main-menu {
            list-style-type: none;
        } 
       .main-menu a{
            color: #fff;
            display: block;
            padding: 10px 20px;
        }
        .main-menu a:hover{
            text-decoration: none;
        }
    </style>
</head>
<body>   
<header class="d-flex align-items-center horizontal-padding">
<nav>
    <ul class="d-flex align-items-center justify-content-between main-menu">
        <li><a href="index.php">Все задачи</a></li>
        <?php if(isset($_SESSION['user_id'])): ?>
            <li><a href="index.php?c=task&a=allyour&id=<?php echo $_SESSION['user_id']; ?>">Задачи, поставленные Вами</a></li>
            <li><a href="index.php?c=task&a=allforyou&id=<?php echo $_SESSION['user_id']; ?>">Задачи, поставленные Вам</a></li>
         <!--   <li><a href="index.php?c=task&a=updateOptions&id=<?php echo $_SESSION['user_id']; ?>">Внести изменения в Задачи</a></li>-->
            <li><a href="index.php?c=user&a=logout">Выйти</a></li>
        <?php endif;?>
        <?php if(!isset($_SESSION['user_id'])): ?>
            <li><a href="index.php?c=user&a=autorize">Войти</a></li>
            <li><a href="index.php?c=user&a=register">Зарегистрироваться</a></li>
        <?php endif;?>
    </ul>
</nav>
</header>