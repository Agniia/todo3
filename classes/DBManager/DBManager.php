<?php
namespace DBManager; 

class DBManager
{	
	protected $host;
	protected $user;
	protected $pass;
	protected $db;
	protected $dbHandler; 
	
	function __construct($host, $user, $pass, $db)
	{
		$this->host = $host;
		$this->user =  $user;
		$this->pass = $pass;
		$this->db = $db;
		$this->doConnect();
		$this->createTODOTables();
	}
	
	protected function doConnect()
	{
		try
		{
			$this->dbHandler = new \PDO('mysql:host='.$this->host.';dbname='.$this->db, $this->user, $this->pass);
		 	$this->dbHandler->setAttribute( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION );
		 	$this->dbHandler->exec("set names utf8");
	 	}
	    catch (PDOException $e) 
	    {
			 print "Error!: " . $e->getMessage() . "<br/>";
			 die();
		}
	}
	
	public function getDbHandler()
	{
		$this->doConnect();
		return $this->dbHandler;
	}
	public function createTODOTables()
	{
		$this->createTableTasks();
		$this->createTableUsers();
	}
	
	protected function createTableTasks()
	{
		try {
			// $sql ="DROP TABLE IF EXISTS `tasks`";
			 $sql ="CREATE TABLE IF NOT EXISTS `tasks` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `user_id` int(11) NOT NULL,
				  `assigned_user_id` int(11) DEFAULT NULL,
				  `description` text NOT NULL,
				  `is_done` tinyint(4) NOT NULL DEFAULT '0',
				  `date_added` datetime NOT NULL,
				  PRIMARY KEY (`id`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8";	
			 $res = $this->dbHandler->exec($sql);
		 	} catch (PDOException $e) {
		    print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
	}
	
	protected function createTableUsers()
	{
		try {
			// $sql ="DROP TABLE IF EXISTS `users`";
			 $sql ="CREATE TABLE IF NOT EXISTS `users`(
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `login` varchar(50) NOT NULL,
			  `password` varchar(255) NOT NULL,
			  PRIMARY KEY (`id`)
			  )  ENGINE=InnoDB DEFAULT CHARSET=utf8";	
			 $res = $this->dbHandler->exec($sql);
		 	} catch (PDOException $e) {
		    print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
	}

}