<?php
namespace Model\User; 

class User
{	
	protected $dbHandler;
	
	function __construct($dbInstance)
	{
		$this->dbHandler = $dbInstance->getDbHandler();
	}
		
	public function add()
	{
		var_dump('add Task');
	}
	
	public function update()
	{
		var_dump('update Task');
	}
	
	public function del()
	{
		var_dump('delete Task');
	}
	
	public function setNewDataToUsers($login, $password) 
	{
		if($this->checkHaveLogin($login)){
			return null;
		}// die('Пользователь с таким login уже существует');
		$passwordMD5 = $this->getHashedPassword($password);
		try {
			$stmt = $this->dbHandler->prepare("INSERT INTO users (login, password) VALUES (:login, :password)");
			$stmt->bindParam(':login', $login, \PDO::PARAM_STR);
			$stmt->bindParam(':password', $passwordMD5, \PDO::PARAM_STR);
		    $res = $stmt->execute();
		    return $this->dbHandler->lastInsertId(); 
		} catch (PDOException $e) {
		    print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
	}
	
	public function getHashedPassword($password)
	{
		$hash = md5($password); 
		return $hash;
	}
			
	public function checkHaveLogin($login) 
	{
	    try {
			$stmt = $this->dbHandler->prepare("SELECT * FROM users WHERE login = :login");
			$stmt->bindParam(':login', $login, \PDO::PARAM_STR);
		    $res = $stmt->execute();
		    if($stmt->rowCount() > 0) return true;
		    else return false;
		} catch (PDOException $e) {
		    print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
	}
		
	public function getUser($login)
	{
		try {
		    $sql = "SELECT * FROM users WHERE login = '$login' ";
			return $this->dbHandler->query($sql);
		} catch (PDOException $e) {
		    print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
	}	
			
	public function all()
	{
	    $sql = 'SELECT * FROM users';
		return $this->dbHandler->query($sql);
	}
	
}