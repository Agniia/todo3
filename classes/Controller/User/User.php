<?php
namespace Controller\User; 

class User
{	
	protected $userModel;
	
	function __construct($dbInstance)
	{
//		var_dump('Controller User');
		$this->userModel = new \Model\User\User($dbInstance);
	}
	
	public function autorize()
	{
		if($_SERVER['REQUEST_METHOD'] === 'GET'){
			\Utile::get()->render('user/getin.php', ['header'=>'Войти', 'autorization' =>'1', 'button' =>'Войти']);	
		}
		else if(isset($_POST['user_login']) && isset($_POST['user_passwd'])){
			$user_login = strip_tags($_POST['user_login']);
			$user_passwd = strip_tags($_POST['user_passwd']);
			$user = $this->userModel->getUser($user_login);
			foreach($user as $detail)
	        {
	            if(md5($user_passwd) === $detail['password'])
	            {
	                $_SESSION['user_id'] = $detail['id'];
	                $_SESSION['user_login'] = $detail['login'];
	                $_SESSION['password'] = $user_passwd;
	            }
	            else
	            {
	            	echo 'Неправильный логин или пароль';
	            }
	        }
	       \Utile::get()->render('user/getin.php', ['header'=>'Войти', 'autorization' =>'1', 'button' =>'Войти']);	
		}
	}
	
	public function register()
	{
		if($_SERVER['REQUEST_METHOD'] === 'GET')
		{
			\Utile::get()->render('user/getin.php', ['header'=>'Зарегистрироваться', 'autorization' =>'0', 'button' =>'Зарегистрироваться']);	
		}
		else if(isset($_POST['user_login']) && isset($_POST['user_passwd']))
		{
			$user_login = strip_tags($_POST['user_login']);
			$user_passwd = strip_tags($_POST['user_passwd']);
			$user = $this->userModel->setNewDataToUsers($user_login, $user_passwd);
			if($user)
			{
					$_SESSION['user_id'] = $user;
	                $_SESSION['user_login'] = $user_login;
	                $_SESSION['password'] = $user_passwd;
			}
	       \Utile::get()->render('user/getin.php', ['header'=>'Зарегистрироваться', 'autorization' =>'0', 'button' =>'Зарегистрироваться']);	
		}
	}
		
	public function logout()
	{
		session_unset();
		session_destroy();
		\Utile::get()->render('user/getin.php', ['header'=>'Войти', 'autorization' =>'1', 'button' =>'Войти']);	
	}	
	public function all()
	{
		return $this->userModel->all();
	}
	
}