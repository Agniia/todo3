<?php
namespace Router; 

class Router
{	
	function __construct()
	{
	}
	
	public function getControllerAction($dbHandler)
	{
		if (!isset($_GET['c']) || !isset($_GET['a'])) {
		    $controller = 'Task';
		    $action = 'all';
		} else {
		    $controller = $_GET['c'];
		    $action = $_GET['a'];
		}
		$controllerText =  '\Controller\\'.ucfirst($controller).'\\'.ucfirst($controller);
		 if (class_exists($controllerText)) {
	        $controller = new $controllerText($dbHandler);
	        if (method_exists($controller, $action)) {
	            $controller->$action();
	        }
	    }
	}
	
}