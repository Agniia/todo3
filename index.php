<?php
    session_start ();
    header('Content-Type: text/html; charset=utf-8');
    error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT);
    ini_set('display_errors', 1);

    function autoloader($className)
    {   
       $file = str_replace('\\',DIRECTORY_SEPARATOR,$className);
       $fileName = 'classes'.DIRECTORY_SEPARATOR . $file . '.php'; 
       if(file_exists($fileName))
        {
    		include $fileName;
        }
    }
    spl_autoload_register('autoloader');
    
    class Utile
    {
        static $utile= null;
        public $router;
        public $dbInstance;
        
        function __construct()
    	{
    	}
        
        public static function get()
        {
            if (!self::$utile) {
                self::$utile = new Utile();
            }
            return self::$utile;
        }
        
        public function config()
        {
            $config = include 'config.php';
            return $config;
        }
        
        public function render($template, $params = [])
        {
            $fileTemplate = 'template/'.$template;
            if (is_file($fileTemplate)) {
                ob_start();
                if (count($params) > 0) {
                    extract($params);
                }
                include $fileTemplate;
                echo ob_get_clean();
            }
        }

    }
    $utile = Utile::get();
    $utile->router = new \Router\Router();
    $config = $utile->config();
    $utile->dbInstance = new \DBManager\DBManager($config['host'], 
            $config['user'], $config['pass'], $config['db']);
    $utile->router->getControllerAction($utile->dbInstance);